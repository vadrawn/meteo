import com.google.gson.*;

import java.io.*;
import java.net.URL;

public class Meteo {
    public static void main(String[] args) throws IOException {
        String apiid = "2f4e39b65bf9dc53c26c4323d7cf1182"; // id
        String[] cities = {"524901", "498817", "1850147"}; // Moscow, Saint-Petersburg, Tokyo

        for (int i = 0; i < cities.length; i++) {
            // Get JSON
            URL url = new URL("http://api.openweathermap.org/data/2.5/weather?id=" + cities[i] + "&units=metric&lang=en&APPID=" + apiid);
            BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));

            // Parsing JSON
            JsonElement jelement = new JsonParser().parse(br.readLine());
            JsonObject  jobject = jelement.getAsJsonObject();

            // Filling weather map
            WeatherMap weatherMap = new WeatherMap();
            weatherMap.name = jobject.get("name").toString();
            weatherMap.weather = jobject.getAsJsonArray("weather").get(0).getAsJsonObject().get("main").toString();
            weatherMap.temp = jobject.getAsJsonObject("main").get("temp").toString();
            weatherMap.pressure = jobject.getAsJsonObject("main").get("pressure").toString();
            weatherMap.humidity = jobject.getAsJsonObject("main").get("humidity").toString();
            weatherMap.windSpeed = jobject.getAsJsonObject("wind").get("speed").toString();
            weatherMap.clouds = jobject.getAsJsonObject("clouds").get("all").toString();

            // Print
            System.out.println(weatherMap.toString());
            System.out.println();
        }
    }
}

class WeatherMap {
    String name;
    String weather;
    String temp;
    String pressure;
    String humidity;
    String windSpeed;
    String clouds;

    public String toString() {
        return "City " + name + ":\n" +
                "Weather - " + weather + "\n" +
                "Temperature - " + temp + " C\n" +
                "Atmospheric pressure - " + pressure + " hPa\n" +
                "Humidity - " + humidity + " %\n" +
                "Wind speed - " + windSpeed + " meter/sec\n" +
                "Clouds - " + clouds + " %";
    }
}